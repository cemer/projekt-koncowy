package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPage extends BasePage {

    @FindBy(className = "text-align-left")
    private WebElement welcomeText;

    @FindBy(xpath = "(//*[contains(text(),'Home')])[1]")
    private WebElement homeButton;

    public AccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public boolean isWelcomeTextDisplayed() {
        return welcomeText.isDisplayed();
    }

    public void goToHomePage(){
        homeButton.click();
    }

}

