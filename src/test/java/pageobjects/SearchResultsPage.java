package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchResultsPage extends BasePage {

    public SearchResultsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    @FindBy(xpath = "//div[@class='content-wrapper']")
    private List<WebElement> searchResults;

    @FindBy(className = "text-muted post-heading")
    private WebElement searchResult;

    @FindBy(xpath = "(//*[contains(text(),'Blue Air')])[1]")
    private WebElement blueAirButtonCheck;

    @FindBy(xpath = "(//*[contains(text(),'Air Burkina')])[1]")
    private WebElement airBurkinaButtonCheck;

    @FindBy(xpath = "(//*[contains(text(),'Blue Air')])[2]")
    private WebElement blueAirResultOfFiltering;

    @FindBy(xpath = "(//*[contains(text(),'Air Burkina')])[2]")
    private WebElement airBurkinaResultOfFiltering;

    @FindBy(xpath = "(//*[@class='btn btn-primary btn-sm btn-wide'])[1]")
    private WebElement detailsButton;

    @FindBy(xpath = "(//*[contains(text(),'Book Now')])[1]")
    private WebElement bookNowButton;

    @FindBy(xpath = "(//*[@class='custom-control-label'])[2]")
    private WebElement threeStarsGradeCheckbox;

    @FindBy(xpath = "(//*[contains(text(),'Search')])[4]")
    private WebElement filterSearchButton;

    @FindBy(xpath = "(//*[contains(text(),'No Results Found')])[1]")
    private WebElement noResultsFoundNotification;

    @FindBy(xpath = "(//*[@class='text-secondary'])[1]")
    private WebElement price;

    @FindBy(xpath = "(//*[@class='irs-slider to type_last'])[1]")
    private WebElement sliderPriceTo;


    public int getSearchResultsProductsNumber() {
        return searchResults.size();
    }

    public void filterBlueAir() throws InterruptedException {
        Thread.sleep(6000);
        blueAirButtonCheck.click();
        Thread.sleep(2000);
    }

    public String getTheTextBlueAirFromSearchResult() {
        return blueAirResultOfFiltering.getText();
    }

    public void filterAirBurkina() throws InterruptedException {
        Thread.sleep(6000);
        airBurkinaButtonCheck.click();
        Thread.sleep(2000);
    }

    public String getTheTextFromAirBurkinaSearchResult() {
        return airBurkinaResultOfFiltering.getText();
    }

    public void clickOnDetailsButton() {
        detailsButton.click();
    }

    public void clickOnBookNowButton() {
        bookNowButton.click();
    }

    public void clickThreeStarsGradeCheckbox() {
        threeStarsGradeCheckbox.click();
    }

    public void clickFilterSearchButton() {
        filterSearchButton.click();
    }

    public String checkPrice() {
        String a = price.getText();
        return a.substring(1);
    }
}
