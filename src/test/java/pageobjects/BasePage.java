package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class BasePage {

    final String MAIN_URL = "https://www.phptravels.net";
    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(xpath = "(.//*[@id='dropdownCurrency'])[2]")
    WebElement myAccountButton;

    @FindBy(xpath = " //*[@class='dropdown-item active tr']")
    WebElement loginButton;

    @FindBy(xpath = "//*[@class='dropdown-item tr']")
    WebElement logoutButton;

    @FindBy(className = "dropdown-login")
    WebElement demoButton;

    @FindBy(id = "dropdownLangauge")
    WebElement dropdownLangauge;

    @FindBy(id = "en")
    WebElement dropdownLangaugeEnglish;

    @FindBy(id = "es")
    WebElement dropdownLangaugeSpanish;

    @FindBy(id = "fr")
    WebElement dropdownLangaugeFrench;

    @FindBy(id = "de")
    WebElement dropdownLangaugeGerman;

    @FindBy(xpath = "//*[@class='dropdown-item tr']")
    WebElement signUp;

    @FindBy(className = "select2-choice")
    WebElement destinationLabel;

    @FindBy(xpath = "(.//*[@class='btn btn-primary btn-block'])[1]")
    WebElement searchButton;

    @FindBy(xpath = "(.//*[@class='text-center hotels active'])[1]")
    WebElement hotelButton;

    @FindBy(xpath = "//*[@id=\"search\"]/div/div/div/div/div/nav/ul/li[2]/a")
    WebElement flightsButton;

    @FindBy(xpath = "//*[@id='s2id_location_from']")
    WebElement flightsFromLabel1;

    @FindBy(xpath = "//*[@id='select2-drop']/div/input")
    WebElement flightsFromLabel2;

    @FindBy(xpath = "//*[@id='s2id_location_to']")
    WebElement flightsToLabel1;

    @FindBy(xpath = "//*[@id='select2-drop']/div/input")
    WebElement flightsToLabel2;

    @FindBy(xpath = "//*[@id='FlightsDateStart']")
    WebElement departDay;

    @FindBy(xpath = "//*[@id=\"flights\"]/div/div/form/div/div/div[3]/div[4]/button")
    WebElement flightsSearchButton;

    @FindBy(className = "d-block")
    WebElement featuredFlights;

    @FindBy(xpath = "//*[@class='btn btn-text-inherit btn-interactive']")
    WebElement currencyButton;

    @FindBy(linkText = "RUB")
    WebElement currencyChangeToRubButton;

    @FindBy(linkText = "GBP")
    WebElement currencyChangeToGbpButton;

    @FindBy(linkText = "KWD")
    WebElement currencyChangeToKwdButton;

    @FindBy(linkText = "JPY")
    WebElement currencyChangeToJPYButton;

    @FindBy(xpath = "//*[@class='select2-no-results']")
    WebElement noResultsSearch;



    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get(MAIN_URL);
    }

    public void goToLogin() {
        myAccountButton.click();
        loginButton.click();
    }

    public void clickLogoutButton() {

        boolean staleElement = true;

        while (staleElement) {

            try {

                demoButton.click();
                logoutButton.click();

                staleElement = false;


            } catch (StaleElementReferenceException e) {

                staleElement = true;

            }

        }
    }

    public void changeLanguageToEnglish() {
        dropdownLangauge.click();
        dropdownLangaugeEnglish.click();
    }

    public void changeLanguageToSpanish() {
        dropdownLangauge.click();
        dropdownLangaugeSpanish.click();
    }

    public void changeLanguageToFrench() {
        dropdownLangauge.click();
        dropdownLangaugeFrench.click();
    }

    public void changeLanguageToGerman() {
        dropdownLangauge.click();
        dropdownLangaugeGerman.click();
    }

    public String getLanguageText(){
        return dropdownLangauge.getText();
    }


    public void goToSignUp() {
        myAccountButton.click();
        signUp.click();
    }

    public void searchForHotel(String nameOfDestination) throws InterruptedException {
        destinationLabel.click();
        Thread.sleep(1000);
        destinationLabel.sendKeys(nameOfDestination);
        Thread.sleep(1000);
        destinationLabel.sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        searchButton.click();
    }


    public void searchForFlight(String cityFrom, String nameOfDestination) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(flightsButton));
        flightsButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(flightsFromLabel1));
        flightsFromLabel1.click();
        flightsFromLabel2.sendKeys(cityFrom);
        Thread.sleep(1000);
        flightsFromLabel2.sendKeys(Keys.ENTER);
        Thread.sleep(1000);
        wait.until(ExpectedConditions.elementToBeClickable(flightsToLabel1));
        flightsToLabel1.click();
        Thread.sleep(1000);
        flightsToLabel2.sendKeys(nameOfDestination);
        Thread.sleep(1000);
        flightsToLabel2.sendKeys(Keys.ENTER);
    }

    public void chooseTheDayOfFlightSetAsTomorrow() {
        departDay.click();
        departDay.sendKeys(Keys.ARROW_RIGHT);
        departDay.sendKeys(Keys.ENTER);
    }

    public void clickSearchFlight() {
        flightsSearchButton.click();
    }

    public void changeCurrencyToRub() throws InterruptedException {
        currencyButton.click();
        currencyChangeToRubButton.click();
        Thread.sleep(2000);
    }

    public void changeCurrencyToGbp() throws InterruptedException {
        currencyButton.click();
        currencyChangeToGbpButton.click();
        Thread.sleep(2000);
    }

    public void changeCurrencyToKwd() throws InterruptedException {
        currencyButton.click();
        currencyChangeToKwdButton.click();
        Thread.sleep(2000);
    }

    public void changeCurrencyToJpy() throws InterruptedException {
        currencyButton.click();
        currencyChangeToJPYButton.click();
        Thread.sleep(2000);
    }


    public String getFeaturedHotelsNumber(){
        return featuredFlights.getText();
    }


    public String getTextOfSearchWithNoResults() {
        return noResultsSearch.getText();
    }

    public void searchForHotelWithNoResult(String nameOfDestination) throws InterruptedException {
        destinationLabel.click();
        Thread.sleep(1000);
        destinationLabel.sendKeys(nameOfDestination);
        Thread.sleep(1000);
    }

}

