package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BookingPage extends BasePage {
    public BookingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "(//*[contains(text(),'Select Room')])[2]")
    private WebElement selectRoomCheckbox;

    @FindBy(xpath = "(//*[contains(text(),'Book Now')])[1]")
    private WebElement bookNowButton;

    @FindBy(xpath = "//*[@class=\"btn btn-success btn-lg btn-block completebook\"]")
    private WebElement confirmBookingButton;

    @FindBy(xpath = "(//*[contains(text(),'Booking Details')])[1]")
    private WebElement bookingDetails;

    @FindBy(xpath = "//*[@class='alert alert-danger']")
    private WebElement alertNotification;

    @FindBy(xpath = "(//*[@name='email'])[1]")
    private WebElement emailInput;

    @FindBy(xpath = "(//*[@name='address'])[1]")
    private WebElement addressInput;

    public void clickOnSelectRoomCheckboxAndBookNowButton() {
        selectRoomCheckbox.click();
        bookNowButton.click();
    }

    public void confirmBooking() throws InterruptedException {
        confirmBookingButton.click();
        Thread.sleep(5000);
    }
    public String getBookingDetailsText() {
       return bookingDetails.getText();
    }

    public String getAlertNotificationText() {
        return alertNotification.getText();
    }

    public String getTypeOfEmailInput() {
        return emailInput.getAttribute("type");
    }

    public String getTypeOfAddressInput() {
        return addressInput.getAttribute("type");
    }

}
