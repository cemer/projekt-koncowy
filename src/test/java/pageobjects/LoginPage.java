package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.SourceType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(name = "username")
    private WebElement loginField;

    @FindBy(name = "password")
    private WebElement passwordField;

    @FindBy(className = "btn-l")
    private WebElement submitLoginButton;

    @FindBy(className = "alert-danger")
    private WebElement authAlert;

    @FindBy(xpath = "(//*[@class='col-md-12'])[1]")
    private WebElement signUpButton;

    @FindBy(xpath = "(//*[contains(text(),'Forget Password')])[1]")
    private WebElement forgetPasswordButton;

    @FindBy(id = "resetemail")
    private WebElement resetPasswordMail;

    @FindBy(xpath = "(//*[@class='alert alert-success'])[1]")
    private WebElement resetPasswordNotificationPositive;

    @FindBy(xpath = "(//*[@class='alert alert-danger'])[1]")
    private WebElement resetPasswordNotificationNegative;

    @FindBy(xpath = "//*[contains(@href,'bootstrap')]")
    private WebElement tooltip;


    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void login(String email, String password) {
        loginField.sendKeys(email);
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.ENTER);
    }

    public boolean isInvalidEmailOrPasswordAlertDisplayed() {
        return authAlert.getText().contains("Invalid Email or Password");
    }

    public boolean isILoginFieldDisplayed() {
        return loginField.isDisplayed();
    }


    public void forgetPasswordClick() throws InterruptedException {
        forgetPasswordButton.click();
        Thread.sleep(2000);
    }

    public void setEmailForForgottenPassword(String email) {
        resetPasswordMail.sendKeys(email);
        resetPasswordMail.sendKeys(Keys.ENTER);
    }

    public String isPositiveNotificationForgottenPasswordSentDisplayed() {
        return resetPasswordNotificationPositive.getText();
    }

    public String isNegativeNotificationForgottenPasswordSentDisplayed() {
        return resetPasswordNotificationNegative.getText();
    }

    public String getTooltipText() {
        Actions action = new Actions(driver);
        action.moveToElement(loginField).build().perform();
        return tooltip.getText();
    }

    public String getTypeOfLoginInput() {
        return loginField.getAttribute("type");
    }

    public String getTypeOfPasswordInput() {
        return passwordField.getAttribute("type");
    }

}


