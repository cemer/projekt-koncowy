package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

public class RegisterPage extends BasePage {

    private String createEmail;

    @FindBy(name = "firstname")
    private WebElement customer_firstname;

    @FindBy(name = "lastname")
    private WebElement customer_lastname;

    @FindBy(name = "phone")
    private WebElement phone_mobile;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "password")
    private WebElement passwd;

    @FindBy(name = "confirmpassword")
    private WebElement confirmPassword;

    @FindBy(xpath = "(//*[@class='alert alert-danger'])[1]")
    private WebElement alertNotification;


    public RegisterPage(String createEmail, WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        this.createEmail = createEmail;
    }

    public void register(RandomUser user) {
        customer_firstname.sendKeys(user.firstName);
        customer_lastname.sendKeys(user.lastName);
        email.sendKeys(user.email);
        passwd.sendKeys(user.password);
        confirmPassword.sendKeys(user.password);
        this.phone_mobile.sendKeys(user.phone);
        this.phone_mobile.sendKeys(Keys.ENTER);
    }

    public void registerWithProvidedValues(String firstName, String lastName, String emailValue, String passwordValue, String confirmPasswordValue, String phoneValue) {
        customer_firstname.sendKeys(firstName);
        customer_lastname.sendKeys(lastName);
        email.sendKeys(emailValue);
        passwd.sendKeys(passwordValue);
        confirmPassword.sendKeys(confirmPasswordValue);
        this.phone_mobile.sendKeys(phoneValue);
        this.phone_mobile.sendKeys(Keys.ENTER);
    }


    public String getAlertNotificationText() {
        return alertNotification.getText();
    }

    public String getTypeOfEmailInput() {
        return email.getAttribute("type");
    }

    public String getTypeOfPasswordInput() {
        return passwd.getAttribute("type");
    }


}
