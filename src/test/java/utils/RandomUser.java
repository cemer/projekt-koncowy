package utils;

import com.github.javafaker.Faker;

public class RandomUser {
    public String firstName;
    public String lastName;
    public String password = "1qaz!QAZ";
    public String phone;
    public String email;

    public RandomUser() {
        Faker faker = new Faker();
        this.firstName = faker.name().firstName();
        this.lastName = faker.name().lastName();
        this.phone = String.valueOf(faker.phoneNumber().cellPhone());
        this.email = this.firstName + this.lastName + faker.random().nextInt(100000) + "@wp.pl";
    }

    @Override
    public String toString() {
        return "RandomUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
