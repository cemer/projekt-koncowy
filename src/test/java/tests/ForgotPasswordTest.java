package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class ForgotPasswordTest extends BaseTest {

    @Test
    void shouldDisplayNoAccountRegisteredForThisEmailWhenUnregisteredEmailProvided() throws InterruptedException {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        Thread.sleep(2000);
        LoginPage loginPage = new LoginPage(driver,wait);
        loginPage.forgetPasswordClick();
        loginPage.setEmailForForgottenPassword("aaa");
        Assertions.assertTrue(loginPage.isNegativeNotificationForgottenPasswordSentDisplayed().contains("Email Not Found"));
    }

    @Test
    void shouldDisplaySuccessAlertWhenRegisteredEmailProvided() throws InterruptedException {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        Thread.sleep(2000);
        LoginPage loginPage = new LoginPage(driver,wait);
        loginPage.forgetPasswordClick();
        loginPage.setEmailForForgottenPassword("user@phptravels.com");
        Assertions.assertTrue(loginPage.isPositiveNotificationForgottenPasswordSentDisplayed().contains("New Password sent to"));
    }
}
