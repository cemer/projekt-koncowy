package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;

public class ChangeCurrencyTest extends BaseTest{

    @Test
    void shouldChangeCurrencyToRub() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeCurrencyToRub();
        Assertions.assertTrue(basePage.getFeaturedHotelsNumber().contains("RUB"));
    }

    @Test
    void shouldChangeCurrencyToGbp() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeCurrencyToGbp();
        Assertions.assertTrue(basePage.getFeaturedHotelsNumber().contains("GBP"));
    }

    @Test
    void shouldChangeCurrencyToKwd() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeCurrencyToKwd();
        Assertions.assertTrue(basePage.getFeaturedHotelsNumber().contains("KWD"));
    }

    @Test
    void shouldChangeCurrencyToJpy() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeCurrencyToJpy();
        Assertions.assertTrue(basePage.getFeaturedHotelsNumber().contains("JPY"));
    }
}
