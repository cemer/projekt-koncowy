package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;

public class ChangeLanguageTest extends BaseTest{

    @Test
    void shouldChangeLanguageToEnglish() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeLanguageToEnglish();
        Assertions.assertEquals(basePage.getLanguageText(),"ENGLISH");
}

    @Test
    void shouldChangeLanguageToSpanish() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeLanguageToSpanish();
        Assertions.assertEquals(basePage.getLanguageText(),"SPANISH");
    }

    @Test
    void shouldChangeLanguageToFrench() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeLanguageToFrench();
        Assertions.assertEquals(basePage.getLanguageText(),"FRENCH");
    }

    @Test
    void shouldChangeLanguageToGerman() {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.changeLanguageToGerman();
        Assertions.assertEquals(basePage.getLanguageText(),"GERMAN");
    }
}
