package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;

public class BookingHotelTest extends BaseTest {

    @Test
    void shouldProperlyBookAHotel() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver,wait);
        loginPage.login("user@phptravels.com", "demouser");
        Thread.sleep(3000);
        AccountPage accountPage = new AccountPage(driver,wait);
        accountPage.goToHomePage();
        basePage.searchForHotel("dubai");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        searchResultsPage.clickOnDetailsButton();
        BookingPage bookingPage = new BookingPage(driver,wait);
        bookingPage.clickOnSelectRoomCheckboxAndBookNowButton();
        bookingPage.confirmBooking();
        Assertions.assertTrue(bookingPage.getBookingDetailsText().contains("Booking Details"));
    }
}