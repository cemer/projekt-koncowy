package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class LogoutTest extends BaseTest{

    @Test
    void shouldLogoutUserWhenLogoutButtonIsClicked() throws InterruptedException {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver,wait);
        loginPage.login("user@phptravels.com", "demouser");
        Thread.sleep(5000);
        basePage.clickLogoutButton();
        Assertions.assertTrue(loginPage.isILoginFieldDisplayed());
    }
}
