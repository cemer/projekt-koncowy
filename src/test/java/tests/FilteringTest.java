package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.SearchResultsPage;

public class FilteringTest extends BaseTest {

    @Test
    void shouldFilterFlightsForOnlyBlueAirAirline() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("lhe", "dxb");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);
        searchResultsPage.filterBlueAir();
        Assertions.assertFalse(searchResultsPage.getTheTextBlueAirFromSearchResult().contains("Air Burkina"));
    }

    @Test
    void shouldFilterFlightsForOnlyAirBurkinaAirline() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("lhe", "dxb");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);
        searchResultsPage.filterAirBurkina();
        Assertions.assertFalse(searchResultsPage.getTheTextFromAirBurkinaSearchResult().contains("Air Blue"));
    }

    @Test
    void shouldFilterHotelsForStarGrade() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForHotel("dubai");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);
        searchResultsPage.clickThreeStarsGradeCheckbox();
        searchResultsPage.clickFilterSearchButton();
        Assertions.assertTrue(searchResultsPage.getSearchResultsProductsNumber() > 0);
    }

    @Test
    void shouldFilterHotelsForPrice() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("lhe", "dxb");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver, wait);

//Zaimplementować move slidera
        Assertions.assertTrue(Integer.parseInt(searchResultsPage.checkPrice()) > 100);
    }
}
