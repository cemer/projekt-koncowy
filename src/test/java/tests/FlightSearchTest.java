package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.SearchResultsPage;

public class FlightSearchTest extends BaseTest{

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForFlight() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("gdn", "bqh");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        Assertions.assertTrue(searchResultsPage.getSearchResultsProductsNumber() > 0);
    }

}
