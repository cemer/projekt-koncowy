package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.SearchResultsPage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HotelSearchTest extends BaseTest {

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForExistingProduct() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForHotel("dubai");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        Assertions.assertTrue(searchResultsPage.getSearchResultsProductsNumber() > 0);
    }

    @Test
    void shouldReturnEmptySearchResultsWhenLookingForNonExistingProduct() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForHotelWithNoResult("drill");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        Assertions.assertTrue(searchResultsPage.getTextOfSearchWithNoResults().contains("No matches found"));
    }

}
