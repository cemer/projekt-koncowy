package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobjects.AccountPage;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

import java.util.concurrent.TimeUnit;


public class LoginTest extends BaseTest {


    @Test
    void shouldLoginUserWhenCorrectCredentialsAreProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("user@phptravels.com", "demouser");
        AccountPage accountPage = new AccountPage(driver, wait);
        Assertions.assertTrue(accountPage.isWelcomeTextDisplayed());
    }

    @Test
    void shouldDisplayInvalidEmailOrPasswordAlertWhenNoEmailIsProvided() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("", "");
        Assertions.assertTrue(loginPage.isInvalidEmailOrPasswordAlertDisplayed());
    }

    @Test
    void inputsShouldHaveCorrectTypes() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver, wait);
        Assertions.assertEquals(loginPage.getTypeOfLoginInput(),"email");
        Assertions.assertEquals(loginPage.getTypeOfPasswordInput(),"password");
    }
}