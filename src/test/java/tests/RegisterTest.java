package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @Test
    void shouldRegisterUserWhenAllMandatoryDataProvided() {
        HomePage basePage = new HomePage(driver, wait);
        basePage.open();
        basePage.changeLanguageToEnglish();
        basePage.goToSignUp();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = new RegisterPage(randomUser.email, driver, wait);
        registerPage.register(randomUser);
        AccountPage accountPage = new AccountPage(driver, wait);
        Assertions.assertTrue(accountPage.isWelcomeTextDisplayed());
    }

    @Test
    void shouldDisplayIncorrectEmailSyntaxNotificationWhenWrongEmailIsProvided() {
        HomePage basePage = new HomePage(driver, wait);
        basePage.open();
        basePage.goToSignUp();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = new RegisterPage(randomUser.email, driver, wait);
        registerPage.registerWithProvidedValues("aaaa", "aaaa",
                "aaaa", "123456", "123456", "12345");
        Assertions.assertTrue(registerPage.getAlertNotificationText().contains("The Email field must contain a valid email address."));
    }

    @Test
    void shouldDisplayPasswordIsTooShortNotificationWhenWrongPasswordIsProvided() {
        HomePage basePage = new HomePage(driver, wait);
        basePage.open();
        basePage.goToSignUp();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = new RegisterPage(randomUser.email, driver, wait);
        registerPage.registerWithProvidedValues("aaaa", "aaaa",
                "aaaa@wp.pl", "1234", "1234", "12345");
        Assertions.assertTrue(registerPage.getAlertNotificationText().contains("The Password field must be at least 6 characters in length."));
    }

    @Test
    void shouldDisplayPasswordIsNotMatchingNotificationWhenWrongPasswordIsProvided() {
        HomePage basePage = new HomePage(driver, wait);
        basePage.open();
        basePage.goToSignUp();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = new RegisterPage(randomUser.email, driver, wait);
        registerPage.registerWithProvidedValues("aaaa", "aaaa",
                "aaaa@wp.pl", "123456", "1234", "12345");
        Assertions.assertTrue(registerPage.getAlertNotificationText().contains("Password not matching with confirm password."));
    }

    @Test
    void inputsShouldHaveCorrectTypes() {
        HomePage homePage = new HomePage(driver, wait);
        homePage.open();
        BasePage basePage = new BasePage(driver, wait);
        basePage.goToSignUp();
        RandomUser randomUser = new RandomUser();
        RegisterPage registerPage = new RegisterPage(randomUser.email, driver, wait);
        Assertions.assertEquals("password", registerPage.getTypeOfPasswordInput());
        Assertions.assertEquals("email", registerPage.getTypeOfEmailInput());
    }
}
