package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.RandomUser;

public class BookingFlightTest extends BaseTest {

    @Test
    void shouldProperlyBookAFlight() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.goToLogin();
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("user@phptravels.com", "demouser");
        Thread.sleep(3000);
        AccountPage accountPage = new AccountPage(driver, wait);
        accountPage.goToHomePage();
        basePage.searchForFlight("gdn", "bqh");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        searchResultsPage.clickOnBookNowButton();
        BookingPage bookingPage = new BookingPage(driver,wait);
        bookingPage.confirmBooking();
        Assertions.assertTrue(bookingPage.getBookingDetailsText().contains("Booking Details"));
    }

    @Test
    void shouldDisplayRequiredFieldsWhenBookAFlightAsAGuestWithEmptyFields() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("gdn", "bqh");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        searchResultsPage.clickOnBookNowButton();
        BookingPage bookingPage = new BookingPage(driver,wait);
        bookingPage.confirmBooking();
        Assertions.assertTrue(bookingPage.getAlertNotificationText().contains("Email is required"));
    }

    @Test
    void inputsShouldHaveCorrectTypes() throws InterruptedException {
        BasePage basePage = new BasePage(driver, wait);
        basePage.open();
        basePage.searchForFlight("gdn", "bqh");
        basePage.chooseTheDayOfFlightSetAsTomorrow();
        basePage.clickSearchFlight();
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver,wait);
        searchResultsPage.clickOnBookNowButton();
        BookingPage bookingPage = new BookingPage(driver,wait);
        Assertions.assertEquals("email", bookingPage.getTypeOfEmailInput());
        Assertions.assertEquals("text", bookingPage.getTypeOfAddressInput());
    }
}